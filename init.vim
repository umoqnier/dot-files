" +++++++++++ Basic Configuration ++++++++++++++

" Filetype detection
filetype plugin on

" Buffers
set hidden

" Indent
set smartindent

" Buffer splits
set splitbelow
set splitright

set incsearch
set encoding=utf-8

" Inteface
set noruler
set showcmd
set noshowmode
set colorcolumn=79
set number
set nowrap
set number relativenumber
set scrolloff=1
set wildmenu
syntax on

" Spelling
set spell spelllang=es_mx
set nospell

" Concel (for md)
set conceallevel=2

" Shortcuts
"""""""""""
" Leader
let mapleader = " "

" split navigations
nnoremap <C-Down> <C-W><C-J>
nnoremap <C-Up> <C-W><C-K>
nnoremap <C-Right> <C-W><C-L>
nnoremap <C-Left> <C-W><C-H>

" Copy to clipboard
noremap <leader>y "+y

" Exit from terminal mode with ESC
tnoremap <Esc> <C-\><C-n>

map<leader>ev :e $MYVIMRC<CR>
map<leader>sv :source $MYVIMRC<CR>

" spellchecking
map <Leader>ss :setlocal spell spelllang=es_mx<CR>
map <Leader>se :setlocal spell spelllang=en_us<CR>
map <Leader>so :set nospell<CR>

"++++++++++++ Plugins Instalation +++++++++++++
call plug#begin()
" NOTE: plugins are installed on ~/.config/nvim/plugged/
" Themes
""""""""
Plug 'chriskempson/base16-vim'
Plug 'mhartington/oceanic-next'
Plug 'chuling/vim-equinusocio-material'
Plug 'NLKNguyen/papercolor-theme'
Plug 'cocopon/iceberg.vim'
Plug 'junegunn/seoul256.vim'

" Interface
""""""""""""
" File explorer
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

" Airline bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Icons
Plug 'ryanoasis/vim-devicons'

"Startify
Plug 'mhinz/vim-startify'

" Distraction free
Plug 'junegunn/goyo.vim'

" Show lines when idention 
" TODO: Issues with markdown
" Plug 'Yggdroot/indentLine'

" Languages utils
"""""""""""""""""
" comments
Plug 'tpope/vim-commentary'
Plug 'scrooloose/nerdcommenter'

" Git integration
Plug 'tpope/vim-fugitive'

" Emmet
Plug 'mattn/emmet-vim'

" Misc Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

"Folding
Plug 'tmhedberg/SimpylFold'

" Automatic quotes, brackets and parentheses completion
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/rainbow_parentheses.vim'

" Code Checker
Plug 'neomake/neomake'

" Lang support
""""""""""""""

" Python
" ******
Plug 'heavenshell/vim-pydocstring', { 'do': 'make install' }

" Autocomplete code

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-jedi'

" For goto functionality
Plug 'davidhalter/jedi-vim'

" Identation
Plug 'vim-scripts/indentpython.vim'

" Code format
Plug 'sbdchd/neoformat'

" Linter (with neomake)
" Rust 
" ****
Plug 'rust-lang/rust.vim'

" Misc langs and syntax / indent
" Plug 'sheerun/vim-polyglot'

" Lisps
Plug 'kovisoft/slimv'

" TODO: learn this shit
Plug 'guns/vim-sexp'

" tpope's bindings make vim-sexp a little nicer to use.
Plug 'tpope/vim-sexp-mappings-for-regular-people'

" Markdown
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

" Pandoc
"Plug 'vim-pandoc/vim-pandoc'
"Plug 'vim-pandoc/vim-pandoc-syntax'

" Editorconfig
Plug 'editorconfig/editorconfig-vim'

" Misc
""""""
"Wakatime
Plug 'wakatime/vim-wakatime'
call plug#end()

"++++++++++++ Plugins Configuration +++++++++++++
" Theme
let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1
colorscheme OceanicNext

" File explorer
nnoremap <Leader>t :NERDTreeToggle<CR>
nnoremap <C-t> :NERDTreeFind<CR>

" Airline
" Airline display buffers on one tab
let g:airline#extensions#tabline#enabled = 1
" Airline file name
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_powerline_fonts = 1
" Airline theme
let g:airline_theme = "oceanicnext"

" Rainbow parents
" Activation based on file type
augroup rainbow_lisp
  autocmd!
  autocmd FileType lisp,clojure,scheme RainbowParentheses
augroup END

" Editorconfig exception for fugitive
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" Virtual envs with python
let g:python3_host_prog = '/home/umoqnier/.pyenv/versions/neovim/bin/python'

" Folding
let g:SimpylFold_docstring_preview=1

" Autocompletion
" ==============

" disable autocompletion, cause we use deoplete for completion
let g:jedi#completions_enabled = 0

" Deoplete
let g:deoplete#enable_at_startup = 1
" Close the method preview
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" Python linter
let g:neomake_python_enabled_makers = ['pylint']
" Automatic checks
" call neomake#configure#automake('nrwi', 500)

" Disable markdown folding
let g:vim_markdown_folding_disabled = 1
