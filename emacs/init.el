;; Package manager stuff
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq package-enable-at-startup nil)

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

;;==GUI==
(setq inhibit-startup-message t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)

(set-fringe-mode 10)

;; Column mode
(column-number-mode t)
;; Show line number
(global-display-line-numbers-mode t)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Set up the visible bell
;(setq visible-bell t)

;; Font size
(set-face-attribute 'default nil :font "BlexMono Nerd Font" :height 200)

;; Theme
(straight-use-package 'modus-themes)
(load-theme 'modus-vivendi t)

;; Modeline
(use-package all-the-icons)

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 10)))

;; Keybindings

;; Make ESC quit promts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Check de syntaxis
(use-package flycheck)

;; Evil mode
(use-package evil
  :config
  (evil-mode 1))

;; Autocompletado de comandos

;; Muestra panel con información de las posibles combinaciones de teclas
(use-package which-key
  :diminish which-key-mode
  :config
  (which-key-mode 1)
  (setq which-key-idle-delay 0.3))

; Menu de opciones de autocompleción
(use-package vertico
  :config
  (vertico-mode 1))

;; ivy alternative
(use-package selectrum
  :config
  (selectrum-mode 1))

(use-package orderless)

(setq selectrum-refine-candidates-function #'orderless-filter)
(setq selectrum-highlight-candidates-function #'orderless-highlight-matches)

;; ivy-rich alternative
(use-package marginalia
  :config
  (marginalia-mode 1))

;; Autocomplete pairs
(setq electric-pair-pairs '(
                           (?\{ . ?\})
                           (?\( . ?\))
                           (?\[ . ?\])
                           (?\¿ . ?\?)
                           (?\¡ . ?\!)
                           (?\" . ?\")
                           (?\$ . ?\$)
                           (?\< . ?\>)
                           (?\« . ?\»)
                           ))

(electric-pair-mode t)

;; Colorful delimiters
(use-package rainbow-delimiters
  :hook
  (prog-mode . rainbow-delimiters-mode))

(add-hook 'org-mode-hook #'rainbow-delimiters-mode)
(add-hook 'lisp-mode-hook #'rainbow-delimiters-mode)
(add-hook 'sly-mode-hook #'rainbow-delimiters-mode)

(use-package sudo-edit)

;;==Languages==

;; Python
(use-package python-mode
  :hook (python-mode . lsp-deferred))

;;==LSP== TODO: Config this properly

(use-package lsp-pyright
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))  ; or lsp-deferred

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))
